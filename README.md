# EEG_SleepClassification

Developed by: Juan David García Hernández and César Daniel Garrido Urbano

***Machine Learning***

Project for the development of the second exam

Classfication of sleep state basen on EEG images

Solving classification problem for https://archive.physionet.org/physiobank/database/sleep-edfx/sleep-cassette/ data


Folders:

Data		    -> Contains scripts (.py) used to read, split, process and manage data. <br>
                -> Contains Data files used to save processed data. <br>
NeuralNetwork 	-> Contains scripts (.py) used to optimze different parameters from trained NeuralNetworks using TensorFlow and Sklearn.<br>
Notebooks 	    -> Contains jupyter notebook files explaining the process and showing the results.<br>

# Librerías
import numpy as np
import math as m
import matplotlib.pyplot as plt
import random

# ---------------------------- Balance de datos ----------------------------
rawX = np.genfromtxt('Data/features.csv',delimiter=',')
rawY = np.genfromtxt('Data/tags.csv',delimiter=',')
# Resumen de datos
def displaySummary():
    awakeCount = len(rawY) - np.count_nonzero(rawY)
    sleep1Count = np.count_nonzero(rawY==1)
    sleep2Count = np.count_nonzero(rawY==2)
    sleepRCount = np.count_nonzero(rawY==3)
    print("Número de elementos clase 0 (Despierto): " + str(awakeCount) + " (" + str(round(100*awakeCount/len(rawY),2)) + " %  del total de datos)")
    print("Número de elementos clase 1 (Sueño ligero): " + str(sleep1Count) + " (" + str(round(100*sleep1Count/len(rawY),2)) + " % del total de datos)")
    print("Número de elementos clase 2 (Sueño profundo): " + str(sleep2Count) + " (" + str(round(100*sleep2Count/len(rawY),2)) + " % del total de datos)")
    print("Número de elementos clase 3 (Sueño REM): " + str(sleepRCount) + " (" + str(round(100*sleepRCount/len(rawY),2)) + " % del total de datos)")
# Balance de datos
displaySummary()
print("Balanceando datos...")
delIndex = []
for i, y in enumerate(rawY):
    r = random.random()
    if y == 0 and r > 0.25:
        delIndex.append(i)
rawY = np.delete(rawY,delIndex)
rawX = np.delete(rawX,delIndex, axis=0)
displaySummary()

# ---------------------------- Separación de datos ----------------------------
from sklearn.model_selection import train_test_split
# Se separan los datos de entrenamiento
XTrain, newRawX, Y, newRawY = train_test_split(rawX, rawY, test_size=0.20)
# Se separan los datos de validación y prueba
XTest, XVal, YTest, YVal = train_test_split(newRawX, newRawY, test_size=0.50)
# Se convierten en vectores columna
YTest = YTest.reshape(-1, 1)
YVal = YVal.reshape(-1, 1)

# ---------------------------- Preprocesamiento ----------------------------
from sklearn import preprocessing
# Se construyen los estandarizadores para cada modelo
scaler = preprocessing.StandardScaler().fit(XTrain)
# Se estandarizan los datos y se guardan en los vectores de entrada de cada modelo
X = scaler.transform(XTrain)

# ---------------------------- Seleccón de Modelo ----------------------------
print("Entrenando One vs. All...")
from sklearn.multiclass import OneVsRestClassifier, OneVsOneClassifier
from sklearn.svm import LinearSVC, SVC
# One VS All (Linear SVM)
model = OneVsRestClassifier(SVC(max_iter=10000, class_weight='balanced'))
model.fit(X,Y)
print("Evaluando One vs. All...")
valError = 1 - model.score(scaler.transform(XVal),YVal)
#trainError = 1 - model.score(scaler.transform(X),Y)
Yova = model.predict(scaler.transform(XVal))
print("Error de validación: " + str(valError))
#print("Error de entrenamiento: " + str(trainError))
# One VS One (linear SVM)
print("Entrenando One vs. One...")
model = OneVsOneClassifier(SVC(gamma='auto', max_iter=10000, class_weight='balanced'))
model.fit(X,Y)
print("Evaluando One vs. One...")
valError = 1 - model.score(scaler.transform(XVal),YVal)
#trainError = 1 - model.score(scaler.transform(X),Y)
Yovo = model.predict(scaler.transform(XVal))
print("Error de validación: " + str(valError))
#print("Error de entrenamiento: " + str(trainError))

# ---------------------------- Matrices de confusion ----------------------------
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
# Función para gráficar matriz de confusión
def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title='Confusion Matrix',
                          cmap=plt.cm.Blues):

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)
    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           title=title,
           ylabel='True label',
           xlabel='Predicted label')
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax

# Gráficas
np.set_printoptions(precision=2)
class_names = ['Despierto', 'Sueño ligero', 'Sueño profundo', 'Sueño REM']
# One vs all
plot_confusion_matrix(YVal, Yova, classes=class_names, normalize=True,
                      title='One vs. All Confusion Matrix')
# One vs one
plot_confusion_matrix(YVal, Yovo, classes=class_names, normalize=True,
                      title='One vs. One Confusion Matrix')

plt.show()

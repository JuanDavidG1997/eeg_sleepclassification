import tensorflow as tf
import numpy as np
import random
import warnings
import sklearn
import keras
import csv
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

# Ignore tensorflow warnings
warnings.simplefilter("ignore")
# Data loading
X = np.genfromtxt('Data/X.csv', delimiter=',')
Y = np.genfromtxt('Data/Y.csv', delimiter=',')
XVal = np.genfromtxt('Data/XVal.csv', delimiter=',')
YVal = np.genfromtxt('Data/YVal.csv', delimiter=',')
XTest = np.genfromtxt('Data/XTest.csv', delimiter=',')
YTest = np.genfromtxt('Data/YTest.csv', delimiter=',')

#X,Y -> Training
#XVal,YVal -> Training
#XTest,YTest -> Training

scaler = StandardScaler()
X = scaler.fit_transform(XTrain)
XTest = scaler.transform(XTest)
XVal = scaler.transform(XVal)

# Converte y to one hot encode
Y = keras.utils.to_categorical(Y)

# Model params
neurons_num = [100, 200, 500, 750, 1000]
layers_num = [1,2,3]
activation_func = ['tanh', 'sigmoid', 'relu', 'identity', 'hard_sigmoid', 'exponential']
exit_func = ['softmax']
epochs = 500
batch_size = 32
num_act_fun = 1
num_exit_func = 1

# Method to get accuracy score over predicted
def get_accuracy_score(y_pred):
    pred = list()
    for i in range(len(y_pred)):
        pred.append(np.argmax(y_pred[i]))
    test = list()
    for i in range(len(YTest)):
        test.append(np.argmax(YTest[i]))
    a = accuracy_score(pred, test)
    print("Acurracy score is: " + str(a))
    return a

# Model creation
# Iterates over several activation functions to fin the best result
for i in range(0, len(activation_func)):
    model = keras.models.Sequential()
    model.add(keras.layers.Dense(neurons_num[4], input_dim=len(X[0]), activation=activation_func[i]))
    model.add(keras.layers.Dense(neurons_num[4], activation=activation_func[i]))
    model.add(keras.layers.Dense(4, activation=exit_func[0]))
    # Model compiler
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    # Trainging
    history = model.fit(X, Y, epochs=epochs, batch_size=batch_size)
    # Predict over validation fraction
    y_pred = model.predict(XVal)
    # Save results in csv file
    with open('Data/results.csv',mode='a') as file:
        writer = csv.writer(file)
        writer.writerow([layers_num[1], neurons_num[4],epochs,activation_func[i],exit_func[0],get_accuracy_score(y_pred)])

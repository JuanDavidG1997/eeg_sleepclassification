import tensorflow as tf
import numpy as np
import random
import warnings
import sklearn
import keras
import csv
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report

# Ignore tensorflow warnings
warnings.simplefilter("ignore")
# Data loading
X = np.genfromtxt('Data/X.csv', delimiter=',')
Y = np.genfromtxt('Data/Y.csv', delimiter=',')
XVal = np.genfromtxt('Data/XVal.csv', delimiter=',')
YVal = np.genfromtxt('Data/YVal.csv', delimiter=',')
XTest = np.genfromtxt('Data/XTest.csv', delimiter=',')
YTest = np.genfromtxt('Data/YTest.csv', delimiter=',')
weigthsY = np.genfromtxt('Data/weigthsY.csv', delimiter=',')
weigthsYVal = np.genfromtxt('Data/weigthsYVal.csv', delimiter=',')
weigthsYTest = np.genfromtxt('Data/weigthsYTest.csv', delimiter=',')


#X,Y -> Training
#XVal,YVal -> Training
#XTest,YTest -> Training

scaler = StandardScaler()
X = scaler.fit_transform(X)
XVal = scaler.transform(XVal)
XTest = scaler.transform(XTest)

# Convert to one hot encode
Y = keras.utils.to_categorical(Y)
YVal = keras.utils.to_categorical(YVal)

# Checking shapes
print("X train shape: " + str(X.shape))
print("Y train shape: " + str(Y.shape))
print("X val shape: " + str(XVal.shape))
print("Y val shape: " + str(YVal.shape))
print("X test shape: " + str(XTest.shape))
print("Y test shape: " + str(YTest.shape))

# Model params
neurons_num = [20,30,40,50,60,70,80,90,100]
layers_num = [5,6]
activation_func = ['tanh', 'sigmoid', 'relu']
exit_func = ['softmax']
epochs = 225
batch_size = 32
num_act_fun = 0
num_exit_func = 0

# Method to obtain accuracy_score
def get_accuracy_score(y_pred):
    pred = list()
    for i in range(len(y_pred)):
        pred.append(np.argmax(y_pred[i]))
    test = list()
    for i in range(len(YVal)):
        test.append(np.argmax(YVal[i]))
    a = accuracy_score(pred, test, sample_weight=weigthsYVal)
    print(classification_report(pred, test, sample_weight=weigthsYVal))
    print("Acurracy score is: " + str(a))
    return a

# Model creation
for i in range(0, len(neurons_num)):
    for j in range(0, len(layers_num)):
        print("Entrenando modelo: " + str(layers_num[j]) + " capas y " + str(neurons_num[i]) + " neuronas.")
        model = keras.models.Sequential()
        model.add(keras.layers.Dense(neurons_num[i], input_dim=len(X[0]), activation=activation_func[num_act_fun]))
        if (layers_num[j] > 1):
            for  k in range(1, layers_num[j]):
                model.add(keras.layers.Dense(neurons_num[i], activation=activation_func[num_act_fun]))
        model.add(keras.layers.Dense(4, activation=exit_func[num_exit_func]))

        # Model compiler
        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
        # Trainging
        history = model.fit(X, Y, epochs=epochs, batch_size=batch_size, validation_data=(XVal, YVal), sample_weight=weigthsY)

        y_pred = model.predict(XVal)
        with open('NeuralNetwork/results.csv',mode='a') as file:
            writer = csv.writer(file)
            writer.writerow([layers_num[j], neurons_num[i],epochs,activation_func[num_act_fun],exit_func[num_exit_func],get_accuracy_score(y_pred)])

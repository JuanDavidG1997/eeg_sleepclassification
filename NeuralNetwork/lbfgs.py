import numpy as np
import warnings
import sklearn
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPClassifier

# Ignore tensorflow warnings
warnings.simplefilter("ignore")
# Data loading
X = np.genfromtxt('Data/X.csv', delimiter=',')
Y = np.genfromtxt('Data/Y.csv', delimiter=',')
XVal = np.genfromtxt('Data/XVal.csv', delimiter=',')
YVal = np.genfromtxt('Data/YVal.csv', delimiter=',')
XTest = np.genfromtxt('Data/XTest.csv', delimiter=',')
YTest = np.genfromtxt('Data/YTest.csv', delimiter=',')

#X,Y -> Training
#XVal,YVal -> Training
#XTest,YTest -> Training

scaler = StandardScaler()
X = scaler.fit_transform(X)
XTest = scaler.transform(XTest)
XVal = scaler.transform(XVal)

n_neurons = [100, 250, 500, 750, 1000]

training_error_lbfgs = []
val_error_lbfgs = []
for i in n_neurons:
    clf = MLPClassifier(solver='lbfgs', hidden_layer_sizes=(i,), max_iter=700, validation_fraction=0)
    clf.fit(X, Y)
    training_error_lbfgs.append(clf.score(X, Y))
    val_error_lbfgs.append(clf.score(XVal, YVal))
    print("Done " + str(i))
best_lbfgs = val_error_lbfgs[np.argmax(val_error_lbfgs)]
print("Best score: " + str(best_lbfgs))
print(val_error_lbfgs)

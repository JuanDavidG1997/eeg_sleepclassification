import numpy as np
from sklearn.model_selection import train_test_split
import random

# Data loading
rawX = np.genfromtxt('Data/features.csv', delimiter=',')
rawY = np.genfromtxt('Data/tags.csv', delimiter=',')

awakeCount = len(rawY) - np.count_nonzero(rawY)
sleep1Count = np.count_nonzero(rawY==1)
sleep2Count = np.count_nonzero(rawY==2)
sleepRCount = np.count_nonzero(rawY==3)

def displaySummary(awakeCount, sleep1Count, sleep2Count, sleepRCount):
    print("Numero de elementos clase 0 (Despierto): " + str(awakeCount) + " (" + str(round(100*awakeCount/len(rawY),2)) + "%  del total de datos)")
    print("Numero de elementos clase 1 (Sueno ligero): " + str(sleep1Count) + " (" + str(round(100*sleep1Count/len(rawY),2)) + "% del total de datos)")
    print("Numero de elementos clase 2 (Sueno profundo): " + str(sleep2Count) + " (" + str(round(100*sleep2Count/len(rawY),2)) + "% del total de datos)")
    print("Numero de elementos clase 3 (Sueno REM): " + str(sleepRCount) + " (" + str(round(100*sleepRCount/len(rawY),2)) + "% del total de datos)")

# Balancing data
displaySummary(awakeCount, sleep1Count, sleep2Count, sleepRCount)
print("Balanceando datos...")
delIndex = []
for i, y in enumerate(rawY):
    r = random.random()
    if y == 0 and r > 0.5:
        delIndex.append(i)
#print(delIndex)
rawY = np.delete(rawY,delIndex)
rawX = np.delete(rawX,delIndex, axis=0)
awakeCount = len(rawY) - np.count_nonzero(rawY)
sleep1Count = np.count_nonzero(rawY==1)
sleep2Count = np.count_nonzero(rawY==2)
sleepRCount = np.count_nonzero(rawY==3)

displaySummary(awakeCount, sleep1Count, sleep2Count, sleepRCount)
# Spliting training data
XTrain, newRawX, YTrain, newRawY = train_test_split(rawX, rawY, test_size=0.20)
# Spliting validation and test data
XTest, XVal, YTest, YVal = train_test_split(newRawX, newRawY, test_size=0.50)
# Shaping into column vectors
YTest = YTest.reshape(-1, 1)
YVal = YVal.reshape(-1, 1)

class_weight = [float(1.0/awakeCount), float(1.0/sleep1Count), float(1.0/sleep2Count), float(1.0/sleepRCount)]

print(class_weight)

weigthsY = []
weigthsYVal = []
weigthsYTest = []
for i in YTrain:
    weigthsY.append(class_weight[int(i)])

for i in YVal:
    weigthsYVal.append(class_weight[int(i)])

for i in YTest:
    weigthsYTest.append(class_weight[int(i)])



#Saving data in csv files
np.savetxt("Data/X.csv", XTrain, delimiter=",")
np.savetxt("Data/Y.csv", YTrain, delimiter=",")
np.savetxt("Data/XVal.csv", XVal, delimiter=",")
np.savetxt("Data/YVal.csv", YVal, delimiter=",")
np.savetxt("Data/XTest.csv", XTest, delimiter=",")
np.savetxt("Data/Ytest.csv", YTest, delimiter=",")
np.savetxt("Data/weigthsY.csv", weigthsY, delimiter=",")
np.savetxt("Data/weigthsYVal.csv", weigthsYVal, delimiter=",")
np.savetxt("Data/weigthsYTest.csv", weigthsYTest, delimiter=",")
# Inform user
print("Datos separados y almacenados de manera correcta...")

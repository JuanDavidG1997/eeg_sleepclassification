import pyedflib as pyedf
import numpy as np
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
import csv
import nitime.algorithms as alg
import pyeeg
from scipy.fftpack import fft
import random as rand

#path = "drive/My Drive/Colab Notebooks/sleep-cassette/"
path = "sleep-cassette/"
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
onlyfiles.sort()

print("Found " + str(len(onlyfiles)) + " files.")

# -------------------------------- FEATURES --------------------------------
# Método para la extracción de características
def extractFeatures(X):
    XFeatures = []
    # Mean absolute value and mean value
    XFeatures.append(np.mean(np.abs(X)))
    XFeatures.append(np.mean(X))
    # Max and min values
    XFeatures.append(np.amax(X))
    XFeatures.append(np.amin(X))
    # Zero-Crossing
    XFeatures.append(len(np.where(np.diff(np.sign(X)))[0]))
    # Slope sign changes
    XFeatures.append(len(np.where(np.logical_and(np.sign(np.diff(X)), np.abs(np.diff(X))>15))[0]))
    # Wave length
    XFeatures.append(np.sum(np.abs(np.diff(X))))
    # Willison Amplitude
    XFeatures.append(np.sum(np.where(np.abs(np.diff(X))>50)[0]))
    # Variance
    XFeatures.append(np.var(X))
    # Log detect
    XFeatures.append(np.exp((np.sum(np.log(np.abs(X))))/len(X)) )
    # Histogram
    hist = np.histogram(X)[0]
    for h in hist:
        XFeatures.append(h)
    # Autoregression Coeficient
    ARCoef = alg.autoregressive.AR_est_LD(X,10)[0]
    for ar in ARCoef:
        XFeatures.append(ar)
    # Cepstrum coefficients
    cepCoef = [-ARCoef[0]]
    sum = 0
    for i in range(1,10):
        sum = 0
        for l in range(1,i-1):
            sum += (1-l/i)*ARCoef[l]*cepCoef[i-1]
        cepCoef.append(-ARCoef[i] - sum)
    for c in cepCoef:
        XFeatures.append(c)

    # Petrosian Fractal Dimension
    XFeatures.append(pyeeg.pfd(X))
    # Higuchi Fractal Dimension
    XFeatures.append(pyeeg.hfd(X,10))
    # Hjorth mobility and complexity
    hjorth = pyeeg.hjorth(X)
    XFeatures.append(hjorth[0])
    XFeatures.append(hjorth[1])
    # Power Spectral Intensity and Relative Intensity Ratio
    band =[0.5, 4, 7, 12, 30]
    bPower = pyeeg.bin_power(X,band,s_FsHz)
    for bp in bPower[1]:
        XFeatures.append(bp)
    # Spectral Entropy (Shannon’s entropy of RIRs)
    sEntropy = pyeeg.spectral_entropy(X,band,s_FsHz, bPower[1])
    XFeatures.append(sEntropy)
    #Fisher Information
    XFeatures.append(pyeeg.fisher_info(X,2,20))
    # Detrended Fluctuation Analysis
    XFeatures.append(pyeeg.dfa(X))
    # HurstExponent(Hurst)
    #XFeatures.append(pyeeg.hurst(X))
    #print('Número de características: ' + str(len(XFeatures)))
    return XFeatures

counter = 0
while counter < len(onlyfiles):
    #auxiliar vectors
    signal = []
    time = []
    tag = []
    print("Loading files: %.2f " % (counter*100/len(onlyfiles)))
    #print("Data/sleep-cassette/" + str(onlyfiles[counter+1]))
    # Reading Hypogram file
    st_FileHypEdf = pyedf.EdfReader(path + str(onlyfiles[counter+1]))
    # Setting time, duration and tags
    v_HypTime, v_HypDur, v_Hyp = st_FileHypEdf.readAnnotations()
    # PSG Data
    st_FileEdf = pyedf.EdfReader(path + str(onlyfiles[counter]))
    s_SigNum = st_FileEdf.signals_in_file
    v_Signal_Labels = st_FileEdf.getSignalLabels()
    s_SigRef = 0
    s_NSamples = st_FileEdf.getNSamples()[0]
    s_FsHz = st_FileEdf.getSampleFrequency(s_SigRef)
    v_Sig = st_FileEdf.readSignal(s_SigRef)
    v_Time = np.arange(0, s_NSamples) / s_FsHz
    # Real vectores
    signalDef = []
    tagDef = []
    # Window span 30 secs
    windowLen = 30

    for i in range(0,len(v_Hyp)-1):
        # Special case
        if i < len(v_Hyp) - 2:
            # Appends each sleep stage separately
            signal.append(v_Sig[(np.where(v_Time==v_HypTime[i])[0][0]):(np.where(v_Time==v_HypTime[i+1])[0][0])])
            time.append(v_Time[(np.where(v_Time==v_HypTime[i])[0][0]):(np.where(v_Time==v_HypTime[i+1])[0][0])])
        else:
            signal.append(v_Sig[(np.where(v_Time==v_HypTime[i])[0][0]):])
            time.append(v_Time[(np.where(v_Time==v_HypTime[i])[0][0]):])
        # Append respective tag
        tag.append(v_Hyp[i])
    # Two iterations given 2 read files
    counter = counter + 2

    #
    for j in range(0, len(time)):
        time[j] = time[j]-time[j][0]
        # Checks important sleep stages
        if tag[j] == 'Sleep stage ?':
            # Jumps to next sleep stage
            continue
        elif tag[j] == 'Movement time':
            # Jumps to next sleep stage
            continue
        for i in range(0, int(time[j][-1]/windowLen)):
            # Special case
            # Appends i'th second window of signal
            if i < (time[j][-1]/windowLen)-1:
                signalDef.append(signal[j][(np.where(time[j]==windowLen*i)[0][0]):(np.where(time[j]==windowLen*(i+1))[0][0])])
                #timeDef.append(time[j][(np.where(time[j]==windowLen*i)[0][0]):(np.where(time[j]==windowLen*(i+1))[0][0])])
            else:
                signalDef.append(signal[j][(np.where(time[j]==windowLen*i)[0][0]):])
                #timeDef.append(time[j][(np.where(time[j]==windowLen*i)[0][0]):])
            # Appends class according to sleep stage
            y = -1
            if tag[j] == 'Sleep stage W':
                y = 0
            elif tag[j] == 'Sleep stage 1' or tag[j] == 'Sleep stage 2':
                y = 1
            elif tag[j] == 'Sleep stage 3' or tag[j] == 'Sleep stage 4':
                y = 2
            elif tag[j] == 'Sleep stage R':
                y = 3
            elif tag[j] == 'Sleep stage ?':
                y = 4
                print("Error")
            else:
                print(tag[j])
                print("Failed at tag setting")
                break
            tagDef.append(y)

    # Feature extraction process
    features = []
    for i in signalDef:
        features.append(extractFeatures(np.array(i)))

    # Writes data file
    with open('Data/features.csv', mode='a') as x_file:
        x_writer = csv.writer(x_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for i in range(0, len(features)):
            x_writer.writerow(features[i])

    #Writes tag file
    with open('Data/tags.csv', mode='a') as y_file:
        y_writer = csv.writer(y_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for i in tagDef:
            y_writer.writerow([i])


# Librerías
import random as rand
import pyedflib as pyedf
import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft

# -------------------------------- ETIQUETAS --------------------------------
# Lectura del archivo de estados de sueño (etiquetas)
st_FileHypEdf = pyedf.EdfReader("sleep-cassette/SC4001EC-Hypnogram.edf")
v_HypTime, v_HypDur, v_Hyp = st_FileHypEdf.readAnnotations()
# Número de ventanas de 30seg
nWindows = v_HypTime[-1]/30
# Vector de etiquetas
YTrain = []
# Se recorre y se llena el vector YTrain
time = 0
index = 0
for i in range(0,int(nWindows)):
    # Si la variable tiempo cambia de etiqueta se aumenta el indice
    if not(time >= v_HypTime[index] and time < v_HypDur[index]) and (index<152):
        index += 1
    # Se cambia la nomenclatura
    label = v_Hyp[index]
    if label == 'Sleep stage W':
        YTrain.append(0)
    elif label == 'Sleep stage 1' or label == 'Sleep stage 2':
        YTrain.append(1)
    elif label == 'Sleep stage 3' or label == 'Sleep stage 4':
        YTrain.append(2)
    if label == 'Sleep stage R':
        YTrain.append(3)
    # Se avanza en el tiempo
    time += 30

# -------------------------------- DATOS --------------------------------
# Vector de Datos
XTrain = []
XTimeTrain = []
# Lectura de las señales s_SigNum señales con nombres v_Signal_Labels
st_FileEdf = pyedf.EdfReader("sleep-cassette/SC4001E0-PSG.edf")
s_SigNum = st_FileEdf.signals_in_file
v_Signal_Labels = st_FileEdf.getSignalLabels()
# Conversion a segundos usando frecuencia de muestreo.
s_SigRef = 0
s_NSamples = st_FileEdf.getNSamples()[0]
s_FsHz = st_FileEdf.getSampleFrequency(s_SigRef)
v_Sig = st_FileEdf.readSignal(s_SigRef)
v_Time = np.arange(0, s_NSamples) / s_FsHz
s_WinSizeSec = 30
s_WinSizeSam = np.round(s_FsHz * s_WinSizeSec)
# Se construye vector de datos y tiempo de vector de datos
s_FirstInd = 0
while 1:
    s_LastInd = s_FirstInd + s_WinSizeSam
    if s_LastInd > s_NSamples:
        break
    XTimeTrain.append(v_Time[s_FirstInd:s_LastInd])
    XTrain.append(v_Sig[s_FirstInd:s_LastInd])
    s_FirstInd = s_LastInd

# -------------------------------- RESUMEN --------------------------------
# Balance de los datos
awakeCount = YTrain.count(0)
sleep1Count = YTrain.count(1)
sleep2Count = YTrain.count(2)
sleepRCount = YTrain.count(3)
print("Número de elementos clase 0 (Despierto): " + str(awakeCount) + " (" + str(round(100*awakeCount/len(YTrain),2)) + " %  del total de datos)")
print("Número de elementos clase 1 (Sueño ligero): " + str(sleep1Count) + " (" + str(round(100*sleep1Count/len(YTrain),2)) + " % del total de datos)")
print("Número de elementos clase 2 (Sueño profundo): " + str(sleep2Count) + " (" + str(round(100*sleep2Count/len(YTrain),2)) + " % del total de datos)")
print("Número de elementos clase 3 (Sueño REM): " + str(sleepRCount) + " (" + str(round(100*sleepRCount/len(YTrain),2)) + " % del total de datos)")

# -------------------------------- Gráficas --------------------------------
# Dominio del tiempo
plt.figure()
plt.title('Muestras aleatorias de EEG para cada clase')
plt.xlabel('Time (sec)')
plt.grid()
randSample = []
indexClass = [index for index, value in enumerate(YTrain) if value == 3]
randSample.append(rand.sample(indexClass, k=1)[0])
indexClass = [index for index, value in enumerate(YTrain) if value == 2]
randSample.append(rand.sample(indexClass, k=1)[0])
indexClass = [index for index, value in enumerate(YTrain) if value == 1]
randSample.append(rand.sample(indexClass, k=1)[0])
indexClass = [index for index, value in enumerate(YTrain) if value == 0]
randSample.append(rand.sample(indexClass, k=1)[0])
# Gráficar datos
for i in randSample:
    plt.plot(XTrain[i])
plt.legend(['Class 3','Class 2','Class 1','Class 0'])
# Dominio de la frecuencia
plt.figure()
plt.title('Muestras aleatorias de EEG en el dominio de la frecuencia')
plt.xlabel('Freq (Hz)')
plt.grid()
for i in randSample:
    XFTrain = 2.0/3000 * np.abs(fft(XTrain[i]))
    plt.plot(XFTrain)
plt.legend(['Class 3','Class 2','Class 1','Class 0'])

plt.show()

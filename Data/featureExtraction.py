# Librerías
import random as rand
import pyedflib as pyedf
import numpy as np
import matplotlib.pyplot as plt
import nitime.algorithms as alg
import pyeeg
from scipy.fftpack import fft

# -------------------------------- ETIQUETAS --------------------------------
# Lectura del archivo de estados de sueño (etiquetas)
st_FileHypEdf = pyedf.EdfReader("sleep-cassette/SC4001EC-Hypnogram.edf")
# Datos en ventanas de 30 segundos,
#v_HypTime es el tiempo de inicio, v_HypDur es la duración en un estado específico (pueden ser varias ventanas),
# v_Hyp es la etiqueta.
v_HypTime, v_HypDur, v_Hyp = st_FileHypEdf.readAnnotations()
#ptl.figure()
#ptl.plot(v_HypTime, v_Hyp)
#ptl.show()

# Número de ventanas de 30seg
nWindows = v_HypTime[-1]/30
# Vector de etiquetas
YTrain = []
# Se recorre y se llena el vector YTrain
time = 0
index = 0
for i in range(0,int(nWindows)):
    # Si la variable tiempo cambia de etiqueta se aumenta el indice
    if not(time >= v_HypTime[index] and time < v_HypDur[index]) and (index<152):
        index += 1
    #print(index)
    # Se cambia la nomenclatura
    label = v_Hyp[index]
    if label == 'Sleep stage W':
        YTrain.append(0)
    elif label == 'Sleep stage 1' or label == 'Sleep stage 2':
        YTrain.append(1)
    elif label == 'Sleep stage 3' or label == 'Sleep stage 4':
        YTrain.append(2)
    if label == 'Sleep stage R':
        YTrain.append(3)
    # Se avanza en el tiempo
    time += 30

# Vector de etiquetas
#print(YTrain)

# -------------------------------- DATOS --------------------------------
# Vector de Datos
XTrain = []
XTimeTrain = []

# Lectura de las señales s_SigNum señales con nombres v_Signal_Labels
st_FileEdf = pyedf.EdfReader("sleep-cassette/SC4001E0-PSG.edf")
s_SigNum = st_FileEdf.signals_in_file
v_Signal_Labels = st_FileEdf.getSignalLabels()

# Conversion a segundos usando frecuencia de muestreo.
s_SigRef = 0
s_NSamples = st_FileEdf.getNSamples()[0]
s_FsHz = st_FileEdf.getSampleFrequency(s_SigRef)

# v_Sig = np.zeros((s_NSamples, 1))
v_Sig = st_FileEdf.readSignal(s_SigRef)
v_Time = np.arange(0, s_NSamples) / s_FsHz
s_WinSizeSec = 30
s_WinSizeSam = np.round(s_FsHz * s_WinSizeSec)

# Se construye vector de datos y tiempo de vector de datos
s_FirstInd = 0
while 1:
    s_LastInd = s_FirstInd + s_WinSizeSam
    if s_LastInd > s_NSamples:
        break

    XTimeTrain.append(v_Time[s_FirstInd:s_LastInd])
    XTrain.append(v_Sig[s_FirstInd:s_LastInd])
    s_FirstInd = s_LastInd

# Vector de datos (Señales)
#print(XTrain)

# -------------------------------- FEATURES --------------------------------
# Método para la extracción de características
def extractFeatures(X):
    # Diff X
    D = np.diff(X)
    # Features vector
    XFeatures = []
    # Mean absolute value and mean value
    XFeatures.append(np.mean(np.abs(X)))
    XFeatures.append(np.mean(X))
    # Max and min values
    XFeatures.append(np.amax(X))
    XFeatures.append(np.amin(X))
    # Zero-Crossing
    XFeatures.append(len(np.where(np.diff(np.sign(X)))[0]))
    # Slope sign changes
    XFeatures.append(len(np.where(np.logical_and(np.sign(D), np.abs(D)>15))[0]))
    # Wave length
    XFeatures.append(np.sum(np.abs(D)))
    # Willison Amplitude
    XFeatures.append(np.sum(np.where(np.abs(D)>52)[0]))
    # Variance
    XFeatures.append(np.var(X))
    # Log detect
    XFeatures.append(np.exp((np.sum(np.log(np.abs(X))))/len(X)) )
    # Histogram
    hist = np.histogram(X)[0]
    for h in hist:
        XFeatures.append(h)
    # Autoregression Coeficient
    ARCoef = alg.autoregressive.AR_est_LD(X,10)[0]
    for ar in ARCoef:
        XFeatures.append(ar)
    # Cepstrum coefficients
    cepCoef = [-ARCoef[0]]
    sum = 0
    for i in range(1,10):
        sum = 0
        for l in range(1,i-1):
            sum += (1-l/i)*ARCoef[l]*cepCoef[i-1]
        cepCoef.append(-ARCoef[i] - sum)
    for c in cepCoef:
        XFeatures.append(c)

    # Petrosian Fractal Dimension
    XFeatures.append(pyeeg.pfd(X,D))
    # Higuchi Fractal Dimension
    XFeatures.append(pyeeg.hfd(X,10))
    # Hjorth mobility and complexity
    hjorth = pyeeg.hjorth(X,list(D))
    XFeatures.append(hjorth[0])
    XFeatures.append(hjorth[1])
    # Power Spectral Intensity and Relative Intensity Ratio
    band =[0.5, 4, 7, 12, 30]
    bPower = pyeeg.bin_power(X,band,s_FsHz)
    for bp in bPower[1]:
        XFeatures.append(bp)
    # Spectral Entropy (Shannon’s entropy of RIRs)
    sEntropy = pyeeg.spectral_entropy(X,band,s_FsHz, bPower[1])
    XFeatures.append(sEntropy)
    #Fisher Information
    XFeatures.append(pyeeg.fisher_info(X,2,20))
    # Detrended Fluctuation Analysis
    XFeatures.append(pyeeg.dfa(X))
    # HurstExponent(Hurst)
    #XFeatures.append(pyeeg.hurst(X))

    print('Número de características: ' + str(len(XFeatures)))
    return XFeatures

# Vector de datos X transformados
newXTrain = []
# Llamado al método de extracción de datos
for i in range(0,1):
    newXTrain.append(extractFeatures(XTrain[i]))
print(newXTrain)
